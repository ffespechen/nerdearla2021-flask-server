# -- Crear un entorno virtual
python -m venv venv

# -- Activarlo
source venv/bin/activate   <-- en GNU Linux
venv\Scripts\activate.bat  <-- en Windows

# -- Instalar Flask
pip install flask

# -- Definir FLASK_APP
export FLASK_APP=server.py  <-- en GNU Linux
set FLASK_APP=server.py     <-- en Windows

# -- Correr la aplicación
flask run --host=0.0.0.0