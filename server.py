from flask import Flask, request

app = Flask(__name__)


@app.route('/')
def index():
    return "Bienvenidos a Flask en #nerdearla 2021"


@app.route('/recibir-json', methods=['POST', 'GET'])
def recibir_json():

    if request.method == 'POST':
        datos = request.get_json()
        print(type(datos))
        print(datos)

        x = datos['y']


        return 'OK', 200
    else:
        return "Estoy escuchando datos en POST... no me distraigan"
